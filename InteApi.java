import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;









import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.sunyard.client.SunEcmClientApi;
import com.sunyard.client.bean.ClientAnnotationBean;
import com.sunyard.client.bean.ClientBatchBean;
import com.sunyard.client.bean.ClientBatchFileBean;
import com.sunyard.client.bean.ClientBatchIndexBean;
import com.sunyard.client.bean.ClientFileBean;
import com.sunyard.client.bean.ClientHeightQuery;
import com.sunyard.client.bean.OperPermission;
import com.sunyard.client.impl.SunEcmClientHttpApiImpl;
import com.sunyard.client.impl.SunEcmClientSocketApiImpl;
import com.sunyard.client.impl.SunEcmClientWebserviceApiImpl;
import com.sunyard.util.OptionKey;
import com.sunyard.util.TransOptionKey;
import com.sunyard.ws.utils.XMLUtil;

/**
 * 客户端使用示例
 * 
 * @author Warren
 * 
 */
public class InteApi {
	//String ip = "188.185.1.95"; //为SunECMDM的ip地址
	String ip = "36.32.160.151"; //为SunECMDM的ip地址
	//int socketPort = 8024;//为SunECMDM的socket端口
	int socketPort = 8021;//为SunECMDM的socket端口
//	int httpPort = 8080; //为SunECMDM的http端口
//	String serverName = "localGroup"; // 连接的服务工程名称
	// String serverName = "UnityAccess"; // 连接的服务工程名称
	String groupName = "ua"; // 内容存储服务器组名
	// SOCKET接口
	SunEcmClientApi clientApi = new SunEcmClientSocketApiImpl(ip, socketPort);

	String STARTCOLUMN = "CREATEDATE";//业务开始时间的属性英文名称
	String STARTDATE = "20201203"; //业务开始时间的值
	// HTTP接口
//	 SunEcmClientApi clientApi = new SunEcmClientHttpApiImpl(ip, httpPort,
//	 serverName);
	// WEBSERVICE接口
//	 SunEcmClientApi clientApi = new SunEcmClientWebserviceApiImpl(ip,
//	 httpPort, serverName);

	// =========================批次信息设定=========================

	//String modelCode = "IKHJCZL"; // 索引对象内容模型代码
	String modelCode = "ZHAPP"; // 索引对象内容模型代码
	String filePartName = "ZHAPP_P"; // 文档对象模型代码
	String userName = "appadmin"; //登录SunECMDM的用户名
	String passWord = "111"; //登录SunECMDM的密码
	String contentID = "2014_1_8FA39BD6-43BD-F384-F3D3-1783228FC800-12"; // 8位日期+随机路径+36位GUID+内容存储服务器ID
	String fileNO1 = "FF1C004F-08FD-88C2-6C8D-2706EC878EAF";
	String fileNO2 = "";
	String fileNO3 = "381CA442-A7DC-1D11-59C0-9B493C997167";

	String annoID = "92E0A6BC-94CA-2F47-3EF3-A57FB34A69B5";

	String checkToken = "caea764b9f8307687edc"; // 检入检出随机数

	String token_check_value = "1234567890";
	String tokenCode = "caea764b9f8307687edc";
	String insNo = "aag";

	// =========================批次信息设定=========================

	public String getContentID() {
		return contentID;
	}

	public void setContentID(String contentID) {
		this.contentID = contentID;
	}

	/**
	 * 登陆
	 * 
	 * @param userName
	 *            用户名
	 * @param password
	 *            密码
	 * @return
	 * @throws Exception
	 */
	public void login() {
		try {
			String resultMsg = clientApi.login(userName, passWord);
			System.out.println("#######登陆返回的信息[" + resultMsg + "]#######");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 登出
	 * 
	 * @param userName
	 *            用户名密码
	 * @return
	 * @throws Exception
	 */
	public void logout() {
		try {
			String resultMsg = clientApi.logout(userName);
			System.out.println("#######登出返回的信息[" + resultMsg + "]#######");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 检出----------------------------------------------------------------------
	 * -------
	 * 
	 * @return
	 * @throws Exception
	 */
	public void checkOut() {
		ClientBatchBean clientBatchBean = new ClientBatchBean();
		clientBatchBean.setModelCode(modelCode);
		clientBatchBean.setUser(userName);
		clientBatchBean.setPassWord(passWord);
		clientBatchBean.getIndex_Object().addCustomMap(STARTCOLUMN, STARTDATE);
		clientBatchBean.getIndex_Object().setContentID(contentID);

		try {
			checkToken = clientApi.checkOut(clientBatchBean, groupName);
			String[] result = checkToken.split("<<::>>");
			checkToken = result[1];
			System.out.println("#######检出返回的信息[" + checkToken + "]#######");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 检入----------------------------------------------------------------------
	 * --
	 * 
	 * @return
	 * @throws Exception
	 */
	public void checkIn() {
		ClientBatchBean clientBatchBean = new ClientBatchBean();
		clientBatchBean.setModelCode(modelCode);
		clientBatchBean.setUser(userName);
		clientBatchBean.setPassWord(passWord);
		clientBatchBean.getIndex_Object().addCustomMap(STARTCOLUMN, STARTDATE);
		clientBatchBean.getIndex_Object().setContentID(contentID);
		clientBatchBean.setCheckToken(checkToken);

		try {
			String resultMsg = clientApi.checkIn(clientBatchBean, groupName);
			System.out.println("#######检入返回的信息[" + resultMsg + "]#######");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 上传接口调用示例 -------------------------------------------------------
	 */

	
	public void uploadExample() {
		ClientBatchBean clientBatchBean = new ClientBatchBean();
		clientBatchBean.setModelCode(modelCode);
		clientBatchBean.setUser(userName);
		clientBatchBean.setPassWord(passWord);
		clientBatchBean.setBreakPoint(false); // 是否作为断点续传上传
		clientBatchBean.setOwnMD5(false); // 是否为批次下的文件添加MD5码
		// 若内容模型配置有安全校验
		// clientBatchBean.setToken_check_value(token_check_value);
//		clientBatchBean.setToken_code(tokenCode);

		// =========================设置索引对象信息开始=========================
		ClientBatchIndexBean clientBatchIndexBean = new ClientBatchIndexBean();
		clientBatchIndexBean.setAmount("1");
		// 索引自定义属性
		 clientBatchIndexBean.addCustomMap("BUSI_SERIAL_NO", "BA20200000003_JJZ");
		clientBatchIndexBean.addCustomMap(STARTCOLUMN, STARTDATE);
		clientBatchIndexBean.addCustomMap("AMOUNT" , "1");
		// =========================设置索引对象信息结束=========================

		// =========================设置文档部件信息开始=========================
		ClientBatchFileBean clientBatchFileBeanA = new ClientBatchFileBean();
		clientBatchFileBeanA.setFilePartName(filePartName);
		// ClientBatchFileBean clientBatchFileBeanB = new ClientBatchFileBean();
		// clientBatchFileBeanB.setFilePartName("LINK_IMG_B");
		// =========================设置文档部件信息结束=========================

		// =========================添加文件=========================
		List<File> fileList = new ArrayList<File>();
		File file = null;
		//for(int i=0;i<1;i++){
		ClientFileBean fileBean1 = new ClientFileBean();
		int SmallWidth=220;
		int SmallHeight=165;
		String path2="C:/list/1.jpg";
		String path2small = path2.substring(0,path2.indexOf("."))+"small"+path2.substring(path2.indexOf("."));
		String path2All = path2.substring(0,path2.indexOf("."))+"All"+path2.substring(path2.indexOf("."));
		String path2Format = path2.substring(path2.indexOf(".")+1);
		String file2=new File(path2).length()+"";
		if(!new File(path2All).exists()){
		    try{
		    	System.out.println(path2+path2small+SmallWidth+SmallHeight);
			saveImageAsJpg(path2, path2small, SmallWidth, SmallHeight);
			joinImages(path2, path2small, path2Format, path2All);
		    } catch (Exception e) {
			    e.printStackTrace();
		    }
		}
		fileBean1.setFileName(path2All);
		fileBean1.setFileFormat(path2Format);
		fileBean1.setFilesize(file2);
		Map<String, String> map = new HashMap<String, String>();
		//map.put("BUSI_FILE_TYPE", "1010201");
		//map.put("TRUENAME", i+".jpg");
		//文件类型
		map.put("FILEFORM", "jpg");
		//文件在控件中的显示名字
		//map.put("SHOWNAME", "");
		fileBean1.setOtherAtt(map);
		clientBatchFileBeanA.addFile(fileBean1);
		//把临时文件放进集合中
		file = new File(path2small);
		fileList.add(file);
		file = new File(path2All);
		fileList.add(file);
		
		// =========================添加文件=========================
		clientBatchBean.setIndex_Object(clientBatchIndexBean);
//		clientBatchBean.setBreakPoint(true);
		clientBatchBean.addDocument_Object(clientBatchFileBeanA);
		// clientBatchBean.addDocument_Object(clientBatchFileBeanB);
		String str = XMLUtil.bean2XML(clientBatchBean);
		System.out.println(str);

		try {
			String resultMsg = clientApi.upload(clientBatchBean, groupName);
			System.out.println("#######上传批次返回的信息[" + resultMsg + "]#######");
		} catch (Exception e) {
			System.out.println("================================================"+clientBatchBean.getIndex_Object().getContentID());
			e.printStackTrace();
		}
	//	finally {
			/*//上传完成之后，删除本地生成的小图以及大小图合并的文件
			if(fileList != null){
				for(int i=0; i<fileList.size(); i++){
					File delfile = fileList.get(i);
					if(delfile.exists()){
						System.out.println("删除文件     ：   " +fileList.get(i));
						delfile.delete();
					}else{
						System.out.println("要删除的文件不存在     ：   " +fileList.get(i));
					}
				}
			}
		}*/
	}
	
	public BufferedImage resize(BufferedImage source, int targetW, int targetH) {
		// targetW，targetH分别表示目标长和宽    
		int type = source.getType();
		BufferedImage target = null;
		double sx = (double) targetW / source.getWidth();
		double sy = (double) targetH / source.getHeight();
		//这里想实现在targetW，targetH范围内实现等比缩放。如果不需要等比缩放    
		//则将下面的if else语句注释即可    
		if (sx > sy) {
			sx = sy;
			targetW = (int) (sx * source.getWidth());
		} else {
			sy = sx;
			targetH = (int) (sy * source.getHeight());
		}
		if (type == BufferedImage.TYPE_CUSTOM) { //handmade    
			ColorModel cm = source.getColorModel();
			WritableRaster raster = cm.createCompatibleWritableRaster(targetW, targetH);
			boolean alphaPremultiplied = cm.isAlphaPremultiplied();
			target = new BufferedImage(cm, raster, alphaPremultiplied, null);
		} else
		target = new BufferedImage(targetW, targetH, type);
		Graphics2D g = target.createGraphics();
		//smoother than exlax:    
		g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		g.drawRenderedImage(source, AffineTransform.getScaleInstance(sx, sy));
		g.dispose();
		return target;
	}
	public void saveImageAsJpg(String fromFileStr, String saveToFileStr, int width, int hight) throws Exception {
		BufferedImage srcImage;
		// String ex = fromFileStr.substring(fromFileStr.indexOf("."),fromFileStr.length());    
		String imgType = "JPEG";
		if (fromFileStr.toLowerCase().endsWith(".jpg")) {
			imgType = "jpg";
		}
		// System.out.println(ex);    
		File saveFile = new File(saveToFileStr);
		File fromFile = new File(fromFileStr);
		System.out.println("****************"+fromFile); 
		
		
		System.out.println("111111111");
		srcImage = ImageIO.read(fromFile);
		System.out.println(srcImage);
		if (width > 0 || hight > 0) {
			srcImage = resize(srcImage, width, hight);
		}
		ImageIO.write(srcImage, imgType, saveFile);

	}
	
	public String joinImages(String firstSrcImagePath,String secondSrcImagePath, String imageFormat, String toPath) throws Exception {
		File fileOne = new File(firstSrcImagePath);
		InputStream in = new FileInputStream(fileOne);
		
		File fileTwo = new File(secondSrcImagePath);
		InputStream in2 = new FileInputStream(fileTwo);
		
		OutputStream fos=new FileOutputStream(toPath);
		
		long len = 0; 
		byte buf[]=new byte[65536];
		int n=0; //记录事迹读取到的字节数
		//循环读取
		while((n=in.read(buf))!=-1)
		{
			len = len + n;
			//输出到指定文件夹
			fos.write(buf,0,n);
		}
		String Biglen=String.valueOf(len);
		System.out.println("大图长度:"+len);
		buf = new byte[65536];
		n=0; //记录事迹读取到的字节数
		len = 0;
		//循环读取
		while((n=in2.read(buf))!=-1)
		{
			len = len + n;
			//输出到指定文件夹
			fos.write(buf,0,n);
		}
		System.out.println("小图长度:"+len);
		in.close();
		in2.close();
		fos.close();
		
		return Biglen;
	}
	/**
	 * 查询接口调用示例 -------------------------------------------------------
	 */
	public void queryExample(String batchId) {
		ClientBatchBean clientBatchBean = new ClientBatchBean();
		clientBatchBean.setModelCode(modelCode);
		clientBatchBean.setUser(userName);
		clientBatchBean.setPassWord(passWord);
		clientBatchBean.setDownLoad(false);
		// clientBatchBean.getIndex_Object().setVersion("1");
		clientBatchBean.getIndex_Object().setContentID(batchId);
		clientBatchBean.getIndex_Object().addCustomMap(STARTCOLUMN,STARTDATE);

		// ClientBatchFileBean documentObjectA = new ClientBatchFileBean();
		// documentObjectA.setFilePartName("LINK_IMG_A"); // 要查询的文档部件名
		// clientBatchBean.addDocument_Object(documentObjectA);

		// ClientBatchFileBean documentObjectB = new ClientBatchFileBean();
		// documentObjectB.setFilePartName("FINA_IMG"); // 要查询的文档部件名
		// documentObjectB.addFilter("FILE_NO",
		// "533E6078-EB43-36E0-1432-6910F643FEA5"); // 增加过滤条件
		// documentObjectB.addFilter("BUSI_FILE_TYPE", "111001"); // 增加过滤条件
		// clientBatchBean.addDocument_Object(documentObjectB);

		// 若内容模型配置有安全校验
		// clientBatchBean.setToken_check_value(token_check_value);
		// clientBatchBean.setToken_code(tokenCode);

		try {
			String resultMsg = clientApi.queryBatch(clientBatchBean, groupName);
			System.out.println("#######查询批次返回的信息[" + resultMsg + "]#######");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 高级搜索调用示例 -------------------------------------------------------
	 * 最后结果为组上最大版本的批次号
	 */
	public void heightQueryExample() {
		ClientHeightQuery heightQuery = new ClientHeightQuery();
		heightQuery.setUserName(userName);
		heightQuery.setPassWord(passWord);
		heightQuery.setLimit(10);
		heightQuery.setPage(1);
		heightQuery.setModelCode(modelCode);
		heightQuery.addCustomAtt(STARTCOLUMN, STARTDATE);
		// heightQuery.addCustomAtt("BUSI_SERIAL_NO", "2013092701");
		// heightQuery.addfilters("VARCHARTYPE='varchartype'");
		try {
			String resultMsg = clientApi.heightQuery(heightQuery, groupName);
			System.out.println("#######调用高级搜索返回的信息[" + resultMsg + "]#######");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除接口调用示例 -------------------------------------------------------
	 */
	public void deleteExample() {
		ClientBatchBean clientBatchBean = new ClientBatchBean();
		clientBatchBean.setModelCode(modelCode);
		clientBatchBean.setPassWord(passWord);
		clientBatchBean.setUser(userName);
		clientBatchBean.getIndex_Object().setContentID(contentID);
		clientBatchBean.getIndex_Object().addCustomMap("BUSI_START_TIME",
				"20130929");
		// 若内容模型配置有安全校验
		// clientBatchBean.setToken_check_value(token_check_value);
		// clientBatchBean.setToken_code(tokenCode);

		try {
			String resultMsg = clientApi.delete(clientBatchBean, groupName);
			System.out.println("#######删除批次返回的信息[" + resultMsg + "]#######");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 更新时需要注明版本号则表示自第几个版本更新,
	 * ------------------------------------------------------- 没有版本控制则无需注明
	 * 
	 */
	public void updateExample(String contentId) {
		ClientBatchBean clientBatchBean = new ClientBatchBean();
		clientBatchBean.setModelCode(modelCode);
		clientBatchBean.setUser(userName);
		clientBatchBean.setPassWord(passWord);
		clientBatchBean.getIndex_Object().setContentID(contentId);
		clientBatchBean.getIndex_Object().addCustomMap(STARTCOLUMN, STARTDATE);
		// clientBatchBean.getIndex_Object().addCustomMap("BUSI_START_TIME",
		// STARTDATE);
		// clientBatchBean.getIndex_Object().addCustomMap("UPDATETIME",
		// "20130828114548");

		// 若内容模型配置有安全校验
		clientBatchBean.setToken_check_value(token_check_value);
		clientBatchBean.setToken_code(tokenCode);
		clientBatchBean.setCheckToken(checkToken);

		ClientBatchFileBean batchFileBean = new ClientBatchFileBean();
		batchFileBean.setFilePartName(filePartName);

		// // 新增一个文件
		/*ClientFileBean fileBean1 = new ClientFileBean();
		fileBean1.setOptionType(OptionKey.U_ADD);
		fileBean1.setFileName("E:/image/2.jpg");
		fileBean1.setFileFormat("jpg");
		batchFileBean.addFile(fileBean1);*/
		// //
		//		
		// // 替换一个文件
		// ClientFileBean clientFileBean2 = new ClientFileBean();
		// clientFileBean2.setOptionType(OptionKey.U_REPLACE);
		// clientFileBean2.setFileNO("B73A7B76-96A8-1094-806F-7730DCFFC024");
		// clientFileBean2.setFileName("D:\\1.jpg");
		// clientFileBean2.setFileFormat("jpg");
		// batchFileBean.addFile(clientFileBean2);

		// 删除一个文件
		ClientFileBean clientFileBean3 = new ClientFileBean();
		clientFileBean3.setOptionType(OptionKey.U_DEL);
		clientFileBean3.setFileNO("8C9B40A9-D99C-57DA-4E29-D6E60DAC8453");
		batchFileBean.addFile(clientFileBean3);
		//		 
		// ClientFileBean clientFileBean4 = new ClientFileBean();
		// clientFileBean4.setOptionType(OptionKey.U_DEL);
		// clientFileBean4.setFileNO("E114898E-9DFE-1E39-8ED4-5C36807455B4");
		// batchFileBean.addFile(clientFileBean4);

		// // 修改文档部件字段
		// ClientFileBean clientFileBean = new ClientFileBean();
		// clientFileBean.setOptionType(OptionKey.U_MODIFY);
		// clientFileBean.setFileNO("B7F0E665-EB2E-A68C-0443-333C2BC80DC4");
		// clientFileBean.addOtherAtt("IMAGEPAGEID", "1");
		// batchFileBean.addFile(clientFileBean);
		// //
		clientBatchBean.addDocument_Object(batchFileBean);
		try {
			String resultMsg = clientApi.update(clientBatchBean, groupName,
					false);
			System.out.println("#######更新批次返回的信息[" + resultMsg + "]#######");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 新增批注
	 * 
	 * @throws Exception
	 */
	public void operAnnotation() {
		// 设定批次信息
		ClientBatchBean clientBatchBean = new ClientBatchBean();
		clientBatchBean.setModelCode(modelCode);
		clientBatchBean.setUser(userName);
		clientBatchBean.getIndex_Object().addCustomMap("BUSI_START_TIME",
				"20120929");
		clientBatchBean.getIndex_Object().setContentID(contentID);
		// 若内容模型配置有安全校验
		clientBatchBean.setToken_check_value(token_check_value);
		clientBatchBean.setToken_code(tokenCode);
		// 设定文档部件信息
		ClientBatchFileBean batchFileBean = new ClientBatchFileBean();
		batchFileBean.setFilePartName(filePartName);

		// 设定文件信息
		ClientFileBean clientFileBean = new ClientFileBean();
		clientFileBean.setFileNO("1BF8004E-957F-EE07-20E3-D6066AF85B99");

		// 追加批注信息
		ClientAnnotationBean annotationBean = new ClientAnnotationBean();
		annotationBean.setAnnotation_id(annoID);
		annotationBean.setAnnotation_flag(OptionKey.U_ADD);
		annotationBean.setAnnotation_x1pos("1");
		annotationBean.setAnnotation_y1pos("1");
		annotationBean.setAnnotation_x2pos("100");
		annotationBean.setAnnotation_y2pos("5");
		annotationBean.setAnnotation_content("内容模型批注");
		annotationBean.setAnnotation_color("red");

		clientFileBean.addAnnoList(annotationBean); // 批注关联文件
		batchFileBean.addFile(clientFileBean); // 文件关联文档部件
		clientBatchBean.addDocument_Object(batchFileBean); // 文档部件关联批次

		try {
			String resultMsg = clientApi.operAnnotation(clientBatchBean,
					groupName);
			System.out.println("#######批注操作返回的信息[" + resultMsg + "]#######");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 查询批注
	 * 
	 * @throws Exception
	 */
	public void queryAnnotation() {
		ClientBatchBean clientBatchBean = new ClientBatchBean();
		clientBatchBean.setModelCode(modelCode);
		clientBatchBean.setUser(userName);
		clientBatchBean.getIndex_Object().setVersion("1");
		clientBatchBean.getIndex_Object()
				.addCustomMap("START_TIME", "20120612");
		clientBatchBean.getIndex_Object().setContentID(contentID);

		// 若内容模型配置有安全校验
		clientBatchBean.setToken_check_value(token_check_value);
		clientBatchBean.setToken_code(tokenCode);

		// 设定文档部件信息
		ClientBatchFileBean batchFileBean = new ClientBatchFileBean();
		batchFileBean.setFilePartName(filePartName);

		// 设定文件信息
		ClientFileBean clientFileBean = new ClientFileBean();
		clientFileBean.setFileNO(fileNO1);

		batchFileBean.addFile(clientFileBean); // 文件关联文档部件
		clientBatchBean.addDocument_Object(batchFileBean); // 文档部件关联批次

		try {
			String resultMsg = clientApi.queryAnnotation(clientBatchBean,
					groupName);
			System.out.println("#######查询批注返回的信息[" + resultMsg + "]#######");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 客户端获得内容模型权限获取 -------------------------------------------------------
	 * */
	public void getPermissions_Client() {
		try {
			String resultMsg = clientApi.getPermissions_Client(userName,
					passWord);
			System.out.println("#######客户端获得内容模型权限获取返回的信息[" + resultMsg + "]#######");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 客户端获取所有服务器信息
	 * 
	 * */
	public void getContentServerInfo_Client() {
		try {
			String resultMsg = clientApi.getContentServerInfo_Client();
			System.out.println("#######客户端获取所有服务器返回的信息[" + resultMsg + "]#######");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 客户端获得获取内容模型列表信息
	 * */
	public void getAllModelMsg_Client() {
		try {
			String resultMsg = clientApi.getAllModelMsg_Client();
			System.out.println("#######客户端获得获取内容模型列表返回的信息[" + resultMsg + "]#######");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 客户端获取内容模型模版
	 * */
	public void getModelTemplate_Client() {

		String[] modeCodes = { modelCode };
		try {
			String resultMsg = clientApi.getModelTemplate_Client(modeCodes);
			System.out.println("#######客户端获取内容模型模版返回的信息[" + resultMsg + "]#######");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 客户端获取内容存储服务器信息
	 * */
	public void inquireDMByGroup() {
		try {
			String resultMsg = clientApi.inquireDMByGroup(userName, groupName);
			System.out.println("#######客户端获取内容存储服务器信息[" + resultMsg + "]#######");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 客户端获取令牌，调用WEBSERVICE
	 * */
	public void getToken() {
		try {
			String oper = OperPermission.ADD + "," + OperPermission.UPD + ","
					+ OperPermission.DEL + "," + OperPermission.QUY + ","
					+ OperPermission.MAK;

			String resultMsg = clientApi.getToken(
					"http://172.16.3.32:9080/SunECMConsole", token_check_value,
					userName, oper);
			System.out.println("#######客户端获取令牌返回的信息[" + resultMsg + "]#######");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void queryAndDownload() {
		ClientBatchBean clientBatchBean = new ClientBatchBean();
		clientBatchBean.setModelCode(modelCode);
		clientBatchBean.setUser(userName);
		// clientBatchBean.getIndex_Object().setVersion("2");
		clientBatchBean.getIndex_Object().setContentID(contentID);
		clientBatchBean.getIndex_Object()
				.addCustomMap("START_TIME", "20130821");
		clientBatchBean.setDownLoad(true);
		// 若内容模型配置有安全校验
		clientBatchBean.setToken_check_value(token_check_value);
		clientBatchBean.setToken_code(tokenCode);

		try {
			String resultMsg = clientApi.queryBatch(clientBatchBean, groupName);
			System.out.println("#######查询批次返回的信息[" + resultMsg + "]#######");
			String batchStr = resultMsg.split(TransOptionKey.SPLITSYM)[1];


		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void receiveFileByURL(String urlStr, String fileName) {
		File file = new File("E:\\testfiles\\urlFile\\" + fileName);
		URL url;
		InputStream in = null;
		try {
			url = new URL(urlStr);
			in = url.openStream();
			FileOutputStream fos = new FileOutputStream(file);
			if (in != null) {
				byte[] b = new byte[1024];
				int len = 0;
				while ((len = in.read(b)) != -1) {
					fos.write(b, 0, len);
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("unitedaccess http -- GetFileServer: " + e.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				System.out.println("unitedaccess http -- GetFileServer: "
								+ e.toString());
			}
		}
	}


	public void queryNodeInfoByGroupIdAndInsNo() {
		try {
			String resultMsg = clientApi.queryNodeInfoByGroupIdAndInsNo(
					"FTC_IDX", "jigou");
			System.out.println("#######返回的信息[" + resultMsg + "]#######");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// 多线程客户端
	private Runnable createTask(final int taskID) {
		return new Runnable() {
			public void run() {
				System.out.println("现在进行第[" + taskID + "]个任务");
				// getToken();
				// checkOut(); //88d29fee522dbf173337
//				login();
				for (int i = 0; i < 1; i++) {
//					 updateExample();
//					 uploadExample();
				}
			login();
			}
		};
	}
	

	

	public static void main(String[] args) {
		new InteApi().uploadExample();
		//new InteApi().queryExample("20201203_313_209AF37D-76EA-2343-458F-EADDFC0FB239-1");
		//new InteApi().deleteUp("20200818_308_CA2A5DDC-81D6-93D4-C5D8-9C48B2498FA9-21");
		//new InteApi().updateExample("20200818_308_CA2A5DDC-81D6-93D4-C5D8-9C48B2498FA9-21");
		//new InteApi().deleteUp("20200827_766_BD81B6FC-5E75-8BE1-5AE6-BCB555A02D09-21");
	}
}
